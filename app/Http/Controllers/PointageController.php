<?php

namespace App\Http\Controllers;

use App\Pointage;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class PointageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*if (!Gate::allows('isAdmin')){
            abort(404, "Desole, cette page est innaccéssible");
        }*/
        $pointages = DB::table('pointages')
            ->join('users','users.id', '=', 'pointages.user_id')
            ->select('users.name')
            ->addSelect('pointages.*')
            ->get();
       // dump($pointages);die();
       return view('admin.pointage.index', ['pointages' => $pointages]);
    }

    public function listComptable()
    {
        $listComptables = User::where('user_type','comptable')->get();
        return view('admin.pointage.users', ['listsComptables' => $listComptables]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $current_user = auth()->user()->id;
        $code = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 5);
        return view('comptable.pointage', ['code' => $code, 'current_user' =>$current_user]);
    }

    public function createMatricule()
    {
        $code = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 5);
        return view ('admin.pointage.pointage',['code' => $code]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hash = Hash::make($request->get('password'));
        $user = auth()->user();

        $pointage = new Pointage();
        $pointage->matricule = $request->get('matricule');
        $pointage->user_id = $user->id;
        $pointage->heure_pointage = Carbon::now()->addMinute(-15)->endOfMinute();

        $pointage->password = $hash;
        $pointage->code = $request->get('code');
        $pointage->save();
        return response()->json(['success', 'Data is successfuly added']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function testPointage(Request $request , $id)
    {
        $pointage =Pointage::find($id);
        $matricule = $pointage->matricule = $request->get('matricule');
        $current_user = auth()->user();

        $qb = DB::table('pointages')
            ->where('pointages.matricule','=', $matricule)
            ->first();

        $user_id = $qb->user_id;
        //dump($user_id);die();
        $user_matricule = $qb->matricule;

        if($current_user->id != $user_id && $matricule != $user_matricule ){
            Session::flash('message','Matricule non valide' );
            return Redirect::to('/pointage');

        }else{
            $pointage->user_id = $user_id;
            $pointage->password = $qb->password ;
            $pointage->heure_pointage = Carbon::now()->addMinute(-15)->endOfMinute();
            $pointage->code = $request->get('code');
            $pointage->save();
        }

        return response()->json(['success', 'Data is successfuly added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pointage = Pointage::find($id);
        $user = DB::table('pointages')
            ->join('users','users.id','=', 'pointages.user_id')
            ->where('pointages.id','=',$id)
            ->first();
        //dump($user);die();
        $code = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 5);
        return view('admin.pointage.edit', ['pointage'=> $pointage, 'code'=>$code, 'user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pointage = Pointage::find($id);
        $pointage->matricule = $request->get('matricule');
        $pointage->heure_pointage = Carbon::now()->addMinute(-15)->endOfMinute();
        $pointage->code = $request->get('code');
        $pointage->save();
        return response()->json(['success', 'Data is successfuly added']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
