<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 11/16/18
 * Time: 3:01 PM
 */

if (!function_exists('currentRoute')){
    function currentRoute(...$routes){
        foreach ($routes as $route){
            if (request()->url() == $route){
                return ' active';
            }
        }
    }
}
