<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pointage extends Model
{
    /**
     * @var string
     */
    protected $table = "pointages";

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    protected $heure_pointage = ['heure_pointage'];

    protected $fillable = ['matricule', 'code', 'user_id', 'heure_pointage'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
