<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/pointages', 'PointageController@index')->name('pointages');
Route::get('/users', 'PointageController@listComptable')->name('users');
Route::get('/pointage', 'PointageController@create')->name('pointage');
Route::get('/pointagehoraire', 'PointageController@createMatricule')->name('pointagehoraire');
Route::post('/pointagehoraire', 'PointageController@store');
Route::post('/pointage/{id}', 'PointageController@testPointage');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::post('pointages/{id}/update', 'PointageController@update');
Route::resource('pointages', 'PointageController');
Route::get('404', ['as' => '404', 'uses' => 'ErrorController@notfound']);
