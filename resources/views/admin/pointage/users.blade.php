@extends('admin.layout')

@section('content')
<h1>Liste des comptables</h1>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Users</th>
        <th>Email</th>
        <th>Fonction</th>
    </tr>
    </thead>
    <tbody>
    @foreach($listsComptables as $listsComptable)
    <tr>
        <td>{{ $listsComptable->name }}</td>
        <td>{{ $listsComptable->email }}</td>
        <td>{{ $listsComptable->user_type }}</td>
    </tr>
    @endforeach
    </tbody>
</table>


@endsection
