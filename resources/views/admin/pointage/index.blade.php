@extends('admin.layout')

@section('content')
<h1>Gestion des heures de pointages</h1>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Users</th>
        <th>Numero matricule</th>
        <th>Heure de Pointage</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($pointages as $pointage)
    <tr>
        <td>{{ $pointage->name }}</td>
        <td>{{ $pointage->matricule }}</td>
        {{--<td>{{ $pointage->heure_pointage->format('d/m/Y H:i') }}</td>--}}
        <td>{{ date('H:i', strtotime($pointage->heure_pointage)) }}</td>
        <td>
            <a href="{{action('PointageController@edit', $pointage->id)}}" class="btn btn-primary">Modifier</a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>


@endsection
