{!! Form::model($pointages, [
    'route' => $pointages->id ? ['compatble.update', $pointages] : 'comptable.pointage',
    'method' => $pointages->id ? 'PUT' : 'POST'
]) !!}

<div class="row">
    <div class="col-sm-6">
        <div class="form-group @if($errors->first('matricule')) has-danger @endif">
            {!! Form::label('matricule', 'Matricule') !!}
            {!! Form::text('matricule', null, ['class' => 'form-control']) !!}
            @if($errors->first('matricule'))
                <small class="form-control-feedback">{{ $errors->first('matricule') }}</small>
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group @if($errors->first('slug')) has-danger @endif">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', null, ['class' => 'form-control']) !!}
            @if($errors->first('password'))
                <small class="form-control-feedback">{{ $errors->first('password') }}</small>
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group @if($errors->first('matricule')) has-danger @endif">
            {!! Form::label('code', 'code') !!}
            {!! Form::text('CODE', null, ['class' => 'form-control']) !!}
            @if($errors->first('code'))
                <small class="form-control-feedback">{{ $errors->first('code') }}</small>
            @endif
        </div>
    </div>
</div>
{!! Form::submit($pointages->id ? 'Update' : 'Pointage', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
