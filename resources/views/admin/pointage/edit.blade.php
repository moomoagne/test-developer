@extends('admin.layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-4 offset-lg-4">
                    <div class="card-header bg-primary">
                        <p><h4 style="text-align: center; color: white;"> {{$user->name}}</h4></p>
                    </div>
                    <div class="card-body">

                    </div>
                </div>
                <br>
                <form method="post" action="{{url('/pointage')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="alert alert-success" id="success" style="display: none;">Modification du pointage effectuer avec succees</div>
                    <input type="hidden" value="{{$pointage->id}}" id="pointage_id">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="matricule">Numero Matricule:</label>
                            <input type="text" class="form-control" name="matricule" id="matricule" value="{{$pointage->matricule}}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="code">Reneigner le code suivant : <strong id="label_code">{{$code}}</strong></label>
                            <input type="text" id="input_code" class="form-control" name="code">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4" style="margin-top:60px">
                            <button id="pointageSubmit" type="submit" class="btn btn-success">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
        <script>
            jQuery(function () {
                jQuery('#pointageSubmit').click(function (e) {
                    e.preventDefault();
                    var code = jQuery('strong#label_code').html();
                    var code_input = jQuery('#input_code').val();
                    var pointage_id = jQuery('#pointage_id').val();
                    console.log(code_input);
                    if (code == code_input) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        jQuery.ajax({
                            url: "http://127.0.0.1:8000/pointages/"+ pointage_id +"/update",
                            method: 'post',
                            data: {
                                matricule : jQuery('#matricule').val(),
                                code : jQuery('#input_code').val(),
                            } ,
                            success: function (result) {
                                jQuery('div#success').css('display','block');
                                jQuery('#matricule').val("");
                                jQuery('#password').val("");
                                jQuery('#input_code').val("");
                                jQuery('button#pointageSubmit').css('cursor','none');
                                var code = jQuery('strong#label_code').html("")
                            }
                        });
                    } else
                    {
                        jQuery('#input_code').css('border-color', 'red');
                        jQuery('#input_code').val("Le code est : {{$code}}");
                    }
                });
            });
        </script>
    </div>
@endsection
