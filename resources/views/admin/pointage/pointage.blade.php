@extends('../layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <form method="post" action="{{url('/pointagehoraire')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="alert alert-success" id="success" style="display: none;">Pointage effectuer</div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="matricule">Numero Matricule:</label>
                            <input type="text" class="form-control" name="matricule" id="matricule" placeholder="GDK004887">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="password">Mot de passe</label>
                            <input type="password" id="password" class="form-control" name="password">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="code">Reneigner le code suivant : <strong id="label_code">{{$code}}</strong></label>
                            <input type="text" id="input_code" class="form-control" name="code" placeholder="code">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4" style="margin-top:60px">
                            <button id="pointageSubmit" type="submit" class="btn btn-success">Pointer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
        <script>
            jQuery(function () {
                jQuery('#matricule').val("");
                jQuery('#password').val("");
                jQuery('#input_code').val("");
                jQuery('#pointageSubmit').click(function (e) {
                   e.preventDefault();
                    var code = jQuery('strong#label_code').html();
                    var code_input = jQuery('#input_code').val();
                    //console.log(code_input);
                   if (code == code_input) {
                       $.ajaxSetup({
                           headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                           }
                       });
                       jQuery.ajax({
                           url: "{{url('/pointagehoraire')}}",
                           method: 'post',
                           data: {
                               matricule : jQuery('#matricule').val(),
                               password : jQuery('#password').val(),
                               code : jQuery('#input_code').val(),
                           } ,
                           success: function (result) {
                               //console.log(result);
                               jQuery('div#success').css('display','block');
                               jQuery('#matricule').val("");
                               jQuery('#password').val("");
                               jQuery('#input_code').val("");
                               jQuery('button#pointageSubmit').css('cursor','none');
                           }
                       });
                   } else
                   {
                       jQuery('#input_code').css('border-color', 'red');
                       jQuery('#input_code').val("Le code est : {{$code}}");
                   }
                });
            });
        </script>
        </div>
@endsection
