@extends('../layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::has('message'))
                    <div class="alert alert-error">{{ Session::get('message') }}</div>
                @endif
                <form method="post" action="{{url('/pointage')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="alert alert-success" id="success" style="display: none;">Pointage effectuer avec succées</div>
                    <div class="row">
                        <input type="hidden" id="user_id" value="{{$current_user}}">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="matricule">Numero Matricule:</label>
                            <input type="text" class="form-control" name="matricule" id="matricule" placeholder="GDK004887" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="password">Mot de passe</label>
                            <input type="password" id="password" class="form-control" name="password" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4">
                            <label for="code">Reneigner le code suivant : <strong id="label_code">{{$code}}</strong></label>
                            <input type="text" id="input_code" class="form-control" name="code" placeholder="code" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4" style="margin-top:60px">
                            <button id="pointageSubmit" type="submit" class="btn btn-success">Pointer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
        <script>
            jQuery(function () {
                jQuery('#matricule').val("");
                jQuery('#password').val("");
                jQuery('#input_code').val("");
                jQuery('#pointageSubmit').click(function (e) {
                    e.preventDefault();
                    var code = jQuery('strong#label_code').html();
                    var code_input = jQuery('#input_code').val();
                    var user_id = jQuery('#user_id').val();
                   // var matricule = jQuery('#matricule').val();
                    //console.log(code_input);
                   /* if (code_input == ""){
                        jQuery('#input_code').css('border-color', 'red');
                        jQuery('#input_code').val("Le champ est vide ");
                    }
                    if (matricule == ""){
                        jQuery('#matricule').css('border-color', 'red');
                        jQuery('#matricule').val("Le champ est vide ");
                    } */
                    if (code == code_input) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        jQuery.ajax({
                            url: "http://127.0.0.1:8000/pointage/"+ user_id ,
                            method: 'post',
                            data: {
                                matricule : jQuery('#matricule').val(),
                                password : jQuery('#password').val(),
                                code : jQuery('#input_code').val(),
                            } ,
                            success: function (result) {
                                //console.log(result);
                                jQuery('div#success').css('display','block');
                                jQuery('#matricule').val("");
                                jQuery('#password').val("");
                                jQuery('#input_code').val("");
                                jQuery('button#pointageSubmit').css('cursor','none');
                            },
                            error: function (result) {
                                jQuery('#matricule').val("");
                                jQuery('#matricule').css('border-color', 'red');
                                jQuery('#matricule').attr('placeholder', 'Matricule Incorrect');
                            }
                        });
                    } else
                    {
                        jQuery('#input_code').css('border-color', 'red');
                    }
                });
            });
        </script>
    </div>
@endsection
